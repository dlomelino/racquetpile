export const getFullName = (player) => {
  return `${player.firstName} ${player.lastName}`
}

export const getPlayerAvatar = (player) => {
  return `${player.firstName[0].toUpperCase()}${player.lastName[0].toUpperCase()}`
}
