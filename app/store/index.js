import Vue from 'nativescript-vue'
import Vuex from 'vuex'
import { ObservableArray } from 'tns-core-modules/data/observable-array';

import getters from './getters'
import actions from './actions'
import mutations from './mutations'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    currentTab: 0,
    players: [
      { id: 1, firstName: "Doug", lastName: "Brown" },
      // { id: 2, firstName: "Alex", lastName: "Fohl" },
      // { id: 3, firstName: "Chris", lastName: "Layton" },
      // { id: 4, firstName: "Daniel", lastName: "Lomelino" },
      // { id: 5, firstName: "Dax", lastName: "Lowery" },
      // { id: 6, firstName: "Lisa", lastName: "Martin" },
      // { id: 7, firstName: "Kessa", lastName: "McNaught" },
      // { id: 8, firstName: "Stephanie", lastName: "Smith" },
      // { id: 9, firstName: "Dan", lastName: "Somers" },
      // { id: 10, firstName: "Heidi", lastName: "Somers" },
      // { id: 11, firstName: "David", lastName: "Strom" },
      // { id: 12, firstName: "Ed", lastName: "Ventura" },
      // { id: 13, firstName: "Brandon", lastName: "Wiley" },
      // { id: 14, firstName: "Tony", lastName: "Winkler" }
    ],
    selectedPlayers: []
  },
  getters,
  actions,
  mutations
})
