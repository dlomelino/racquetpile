export default {
  addSelectedPlayers(state, payload) {
    state.selectedPlayers = state.selectedPlayers.concat(payload)
  },
  setCurrentTab(state, payload) {
    state.currentTab = payload
  },
  setSelectedPlayers(state, payload) {
    state.selectedPlayers = payload
  }
}
