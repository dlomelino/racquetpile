import Vue from 'nativescript-vue'
import DateTimePicker from "nativescript-datetimepicker/vue";
import RadListView from 'nativescript-ui-listview/vue';
import App from './components/App'
import store from './store';

import VueDevtools from 'nativescript-vue-devtools'

Vue.use(DateTimePicker)
Vue.use(RadListView)

if(TNS_ENV !== 'production') {
  Vue.use(VueDevtools)
}

// Prints Vue logs when --env.production is *NOT* set while building
Vue.config.silent = (TNS_ENV === 'production')

new Vue({
  store,
  render: h => h(App)
}).$start()
